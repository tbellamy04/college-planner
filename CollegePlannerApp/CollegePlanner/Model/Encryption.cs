﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace CollegePlanner.Model
{
    public class Encryption
    {
        public static byte[] GetKey(string strkey)
        {
            byte[] Key = Convert.FromBase64String(strkey);
            return Key;
        }
        public static string Encyrpt(string data)
        {
            TripleDESCryptoServiceProvider crypto = new TripleDESCryptoServiceProvider();

            crypto.Mode = CipherMode.ECB;
            crypto.Key = GetKey("SkOk8c6O3lDA5MkD90ojXLDGUNDAP7oC");

            crypto.Padding = PaddingMode.PKCS7;
            ICryptoTransform cryptoTransform = crypto.CreateEncryptor();
            Byte[] Buffer = ASCIIEncoding.ASCII.GetBytes(data);

            return Convert.ToBase64String(cryptoTransform.TransformFinalBlock(Buffer, 0, Buffer.Length));
        }
        public static string Decrypt(string data)
        {
            TripleDESCryptoServiceProvider crypto = new TripleDESCryptoServiceProvider();

            crypto.Mode = CipherMode.ECB;
            crypto.Key = GetKey("SkOk8c6O3lDA5MkD90ojXLDGUNDAP7oC");

            crypto.Padding = PaddingMode.PKCS7;
            ICryptoTransform cryptoTransform = crypto.CreateDecryptor();
            Byte[] Buffer = Convert.FromBase64String(data.Replace(" ", "+"));

            return Encoding.ASCII.GetString(cryptoTransform.TransformFinalBlock(Buffer, 0, Buffer.Length));
        }
    }
}
