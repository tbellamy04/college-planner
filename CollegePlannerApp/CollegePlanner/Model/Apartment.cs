﻿using System;

namespace CollegePlanner.Model
{
    public class Apartment
    {
        private int _aptId;

        public int AptId
        {
            get { return _aptId; }
            set { _aptId = value; }
        }

        private int _stateId;

        public int StateId
        {
            get { return _stateId; }
            set { _stateId = value; }
        }

        private string _apartmentSize;

        public string ApartmentSize
        {
            get { return _apartmentSize; }
            set { _apartmentSize = value; }
        }

        private Double _price;

        public Double Price
        {
            get { return _price; }
            set { _price = value; }
        }
    }
}
