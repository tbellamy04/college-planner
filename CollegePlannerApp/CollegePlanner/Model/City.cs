﻿namespace CollegePlanner.Model
{
    public class City
    {
        private int _cityId;

        public int CityId
        {
            get { return _cityId; }
            set { _cityId = value; }
        }

        private int _stateId;

        public int StateId
        {
            get { return _stateId; }
            set { _stateId = value; }
        }

        private string _cityName;

        public string CityName
        {
            get { return _cityName; }
            set { _cityName = value; }
        }
    }
}
