﻿namespace CollegePlanner.Model
{
    public class AccredAgency
    {
        private int _agencyId;

        public int AgencyId
        {
            get { return _agencyId; }
            set { _agencyId = value; }
        }

        private string _agencyName;

        public string AgencyName
        {
            get { return _agencyName; }
            set { _agencyName = value; }
        }
    }
}
