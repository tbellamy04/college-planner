﻿using System;

namespace CollegePlanner.Model
{
    public class CareerEarnings
    {
        public string YearNumber { get; set; }
        public Double YearSalary { get; set; }
    }
}
