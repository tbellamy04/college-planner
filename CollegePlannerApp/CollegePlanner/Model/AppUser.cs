﻿namespace CollegePlanner.Model
{
    public class AppUser
    {
        private string _username;

        public string UserName
        {
            get { return _username; }
            set { _username = value; }
        }

        private string _firstName;

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _lastName;

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private string _password;

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        private int _payscaleId;

        public int PayscaleId
        {
            get { return _payscaleId; }
            set { _payscaleId = value; }
        }
    }
}
