﻿using System;

namespace CollegePlanner.Model
{
    public class Payscale
    {
        private int _payId;

        public int PayId
        {
            get { return _payId; }
            set { _payId = value; }
        }

        private string _major;

        public string Major
        {
            get { return _major; }
            set { _major = value; }
        }

        private Double _startingSalary;

        public Double StartingSalary
        {
            get { return _startingSalary; }
            set { _startingSalary = value; }
        }

        private Double _midSalary;

        public Double MidSalary
        {
            get { return _midSalary; }
            set { _midSalary = value; }
        }
    }
}
