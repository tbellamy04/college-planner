﻿using System;

namespace CollegePlanner.Model
{
    public class School
    {
        private int _schoolId;

        public int SchoolId
        {
            get { return _schoolId; }
            set { _schoolId = value; }
        }

        private string _city;

        public string CityName
        {
            get { return _city; }
            set { _city = value; }
        }

        private string _state;

        public string StateName
        {
            get { return _state; }
            set { _state = value; }
        }

        private string _agency;

        public string AgencyName
        {
            get { return _agency; }
            set { _agency = value; }
        }


        private string _schoolName;

        public string SchoolName
        {
            get { return _schoolName; }
            set { _schoolName = value; }
        }

        private string _schoolUrl;

        public string SchoolUrl
        {
            get { return _schoolUrl; }
            set { _schoolUrl = value; }
        }

        private string _aiddUrl;

        public string AidUrl
        {
            get { return _aiddUrl; }
            set { _aiddUrl = value; }
        }

        private Double _latitude;

        public Double Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }

        private Double _longitude;

        public Double Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }

        private Double _adminRate;

        public Double AdminRate
        {
            get { return _adminRate; }
            set { _adminRate = value; }
        }

        private Double _satAvg;

        public Double SatAvg
        {
            get { return _satAvg; }
            set { _satAvg = value; }
        }

        private Double _actAvg;

        public Double ActAvg
        {
            get { return _actAvg; }
            set { _actAvg = value; }
        }

        private Boolean _distance;

        public Boolean Distance
        {
            get { return _distance; }
            set { _distance = value; }
        }

        private Double _inStateTuition;

        public Double InStateTuition
        {
            get { return _inStateTuition; }
            set { _inStateTuition = value; }
        }

        private Double _outStateTuition;

        public Double OutStateTuition
        {
            get { return _outStateTuition; }
            set { _outStateTuition = value; }
        }

        private Double _estCost;

        public Double EstCost
        {
            get { return _estCost; }
            set { _estCost = value; }
        }

    }
}
