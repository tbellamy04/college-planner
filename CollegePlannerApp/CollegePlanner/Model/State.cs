﻿namespace CollegePlanner.Model
{
    public class State
    {
        private int _stateId;

        public int StateId
        {
            get { return _stateId; }
            set { _stateId = value; }
        }

        private string _stateName;

        public string StateName
        {
            get { return _stateName; }
            set { _stateName = value; }
        }

        public Controller.MController MController
        {
            get => default;
            set
            {
            }
        }
    }
}
