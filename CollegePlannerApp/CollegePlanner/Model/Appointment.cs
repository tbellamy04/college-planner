﻿using System;

namespace CollegePlanner.Model
{
    public class Appointment
    {
        private int _appointmentId;

        public int AppointmentId
        {
            get { return _appointmentId; }
            set { _appointmentId = value; }
        }

        private int _schoolId;

        public int SchoolId
        {
            get { return _schoolId; }
            set { _schoolId = value; }
        }

        private string _schoolName;

        public string SchoolName
        {
            get { return _schoolName; }
            set { _schoolName = value; }
        }


        private string _userName;

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }


        private DateTime _appointmentTime;

        public DateTime AppointmentTime
        {
            get { return _appointmentTime; }
            set { _appointmentTime = value; }
        }

        private string _notes;

        public string Notes
        {
            get { return _notes; }
            set { _notes = value; }
        }
    }
}
