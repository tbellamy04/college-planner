﻿using CollegePlanner.DataAccess.DBProcessors;
using CollegePlanner.Model;
using System;
using System.Collections.Generic;

namespace CollegePlanner.Controller
{
    public class MController
    {
        private static MController _instance;

        protected MController()
        {
            StatesList = GetStateList();
        }

        public static MController Instance()
        {
            if (_instance == null)
            {
                _instance = new MController();
            }
            return _instance;
        }

        public AppUser SignedInUser { get; set; }
        public List<Appointment> UserAppointments { get; set; }
        public List<State> StatesList { get; set; }


        public int Adduser(AppUser appUser)
        {

            if (UserProcessor.CheckUserName(appUser.UserName) == -1)
            {
                return -1;
            }
            return UserProcessor.SaveUser(appUser);
        }
        public void ModifyUser(AppUser appUser)
        {
            UserProcessor.UpdateUser(appUser);
        }

        public int AddAppointment(Appointment data)
        {
            return AppointmentProcessor.SaveAppointment(data);
        }
        public int ModifyAppointment(Appointment data)
        {
            return AppointmentProcessor.UpdateAppointment(data);
        }
        public int DeleteAppointment(Appointment data)
        {
            return AppointmentProcessor.DeleteAppointment(data);
        }
        public int GetAppointmentId()
        {
            return AppointmentProcessor.GetAppointmentId();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>List of Schools from CollegePlannerDB that only have schoolName and schoolId</returns>
        public List<School> GetSchoolNames()
        {
            return SchoolProcessor.GetSchoolNames();
        }
        /// <summary>
        /// Takes in a stateId
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of schools from CollegePlannerDB that are in the state that matches passed in stateId</returns>
        public List<School> GetSchoolByState(int id, Double min, Double max)
        {
            return SchoolProcessor.GetSchoolByState(id, min, max);
        }
        /// <summary>
        /// Takes in a cityId
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns>List of schools from CollegePlannerDB that are in teh city that matches passed in cityId</returns>
        public List<School> GetSchoolByCity(int cityId, Double min, Double max)
        {
            return SchoolProcessor.GetSchoolByCity(cityId, min, max);
        }
        /// <summary>
        /// Takes in a min and max tuition
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns>List of schools from CollegePlannerDB that are with the min and max tuiiton passed in</returns>
        public List<School> GetSchoolByPrice(Double min, Double max)
        {
            return SchoolProcessor.GetSchoolByTuition(min, max);
        }
        /// <summary>
        /// Takes in schoolId
        /// </summary>
        /// <param name="id"></param>
        /// <returns>School from CollegePlannerDB that matches passed in schoolId</returns>
        public School GetSchoolById(int id)
        {
            return SchoolProcessor.GetSchoolById(id);
        }
        /// <summary>
        /// Pulls all payscales from the CollegePlannerDB
        /// </summary>
        /// <returns>List of the payscales that are currently in CollegePlannerDB</returns>
        public List<Payscale> GetPayscaleList()
        {
            return SqlHelper.GetPayscaleList();
        }
        /// <summary>
        /// Pulls all states from CollegePlannerDB
        /// This list is only used as search helper
        /// </summary>
        /// <returns>List of the states that are currently in CollegePlannerDB</returns>
        public List<State> GetStateList()
        {
            return SqlHelper.GetStateList();
        }
        /// <summary>
        /// Pulls all cities in selected state. 
        /// This list are only used as search helpers
        /// </summary>
        /// <returns>List of the cities that are currently in CollegePlannerDB</returns>
        public List<City> GetCitiesByState(int stateId)
        {
            return SqlHelper.GetCityList(stateId);
        }
        /// <summary>
        /// Pulls all appointments for currently logged in user
        /// </summary>
        /// <param name="appUser"></param>
        /// <returns></returns>
        public void LoadAppointmentList()
        {
            UserAppointments = AppointmentProcessor.GetAppointmentList(SignedInUser.UserName);

            foreach (Appointment appointment in UserAppointments)
            {
                appointment.AppointmentTime = AppointmentProcessor.GetDateTime(appointment.AppointmentId);
            }
        }

        /// <summary>
        /// Checks if user exists in system
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns>Return user from CollegePlannerDB if provide values match. Else returns null</returns>
        public AppUser GetUser(string userName, string password)
        {

            AppUser user = new AppUser
            {
                UserName = userName,
                Password = password
            };
            return UserProcessor.GetUser(user);
        }

        public DateTime GetTime(int id)
        {
            return AppointmentProcessor.GetDateTime(id);
        }

        public Double GetSchoolMinPrice()
        {
            return SchoolProcessor.GetMinPrice();
        }
        public Double GetSchoolMaxPrice()
        {
            return SchoolProcessor.GetMaxPrice();
        }
        public List<Apartment> GetApartments()
        {
            return SqlHelper.GetApartmentList();
        }
        public Payscale GetPayscaleById(int id)
        {
            return SqlHelper.GetPayscaleById(id);
        }
        // Need figure how to make a school ranking list that cane be saved to DB

    }
}
