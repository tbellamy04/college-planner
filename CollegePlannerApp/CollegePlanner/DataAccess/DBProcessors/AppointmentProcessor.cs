﻿using CollegePlanner.Model;
using System;
using System.Collections.Generic;

namespace CollegePlanner.DataAccess.DBProcessors
{
    public class AppointmentProcessor
    {
        /// <summary>
        /// Gets the currently logged in users appointments
        /// </summary>
        /// <param name="userName"></param>
        /// <returns>List of appointments that match the passed in userName in CollegePlannerDB</returns>
        public static List<Appointment> GetAppointmentList(string userName)
        {
            string sql = @"SELECT a.appointmentId, a.schoolId, a.userName, a.time, a.notes, s.schoolName
                         FROM Appointment a
                         JOIN School s
                         ON s.schoolId = a.schoolId
                         WHERE userName = @searchValue;";
            return SqlAccess.GetData<Appointment>(sql, userName);
        }
        public static DateTime GetDateTime(int id)
        {
            string sql = "SELECT time FROM Appointment WHERE appointmentId = @value;";
            return SqlAccess.GetSingleEnity<DateTime>(sql, id);
        }

        public static int GetAppointmentId()
        {
            string sql = "SELECT MAX(appointmentId) FROM Appointment;";
            return SqlAccess.GetSingleEnity<int>(sql);
        }

        public static int SaveAppointment(Appointment data)
        {
            string sql = @"INSERT INTO Appointment(schoolId, userName, time, notes)
                          VALUES(@schoolId, @userName, @appointmentTime, @notes);";
            return SqlAccess.UpdataData(sql, data);
        }
        public static int UpdateAppointment(Appointment data)
        {
            string sql = @"UPDATE Appointment SET
                        schoolId = @schoolId,
                        time = @appointmentTime,
                        notes = @notes
                        WHERE appointmentId = @appointmentId;";
            return SqlAccess.UpdataData(sql, data);
        }
        public static int DeleteAppointment(Appointment data)
        {
            string sql = "DELETE FROM Appointment WHERE appointmentId = @appointmentId;";

            return SqlAccess.UpdataData(sql, data);
        }

    }
}
