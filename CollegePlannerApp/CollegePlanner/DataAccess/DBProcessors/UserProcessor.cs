﻿using CollegePlanner.Model;

namespace CollegePlanner.DataAccess.DBProcessors
{
    public class UserProcessor
    {

        public static AppUser GetUser(AppUser data)
        {
            string sql = @"SELECT * FROM AppUser WHERE userName = @value AND password = @value2;";

            AppUser appUser = null;
            appUser = SqlAccess.GetSingleEnity<AppUser>(sql, data.UserName, data.Password);

            if (appUser != null)
            {
                return appUser;
            }

            return null;
        }

        public static int CheckUserName(string username)
        {
            string sql = "SELECT -1 FROM AppUser WHERE userName = @value";
            return SqlAccess.GetSingleEnity<int>(sql, username);
        }

        public static int SaveUser(AppUser data)
        {
            string sql = @"INSERT INTO AppUser(userName, firstName, lastName, password, payscaleId)
                     VALUES(@userName, @firstName, @lastName, @password, @PayscaleId);";

            return SqlAccess.UpdataData(sql, data);
        }

        public static int UpdateUser(AppUser data)
        {
            string sql = @"UPDATE AppUser SET
                  firstName = @firstName,
                  lastName = @lastName,
                  payscaleId = @PayscaleId
                  WHERE userName = @userName AND password = @password;";

            return SqlAccess.UpdataData(sql, data);
        }
    }
}
