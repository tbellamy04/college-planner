﻿using CollegePlanner.Model;
using System;
using System.Collections.Generic;

namespace CollegePlanner.DataAccess.DBProcessors
{
    public class SchoolProcessor
    {

        public static List<School> GetSchoolNames()
        {
            string sql = @"SELECT schoolName, schoolId from School";

            return SqlAccess.GetData<School>(sql);
        }

        public static List<School> GetSchoolByState(int stateId, Double min, Double max)
        {
            string sql = @"SELECT sh.schoolId, c.cityName, s.stateName, a.agencyName, sh.schoolUrl, sh.aidUrl, sh.latitude, 
	                    sh.longitude, sh.adminRate, sh.satAvg, sh.actAvg, sh.distance, sh.inStateTuition, sh.outStateTuition, sh.schoolName
                        FROM School sh
                        JOIN City c on sh.cityId = c.cityId
                        JOIN State s on sh.stateId = s.stateId
                        Join AccredAgency a on sh.agencyId = a.agencyId 
                        WHERE sh.stateId = @value AND sh.OutStateTuition >= @value2 AND sh.OutStateTuition <= @value3 ORDER BY c.cityName;";

            return SqlAccess.GetData<School>(sql, stateId, min, max);
        }

        public static List<School> GetSchoolByCity(int cityId, Double min, Double max)
        {
            string sql = @"SELECT sh.schoolId, c.cityName, s.stateName, a.agencyName, sh.schoolUrl, sh.aidUrl, sh.latitude, 
	                    sh.longitude, sh.adminRate, sh.satAvg, sh.actAvg, sh.distance, sh.inStateTuition, sh.outStateTuition, sh.schoolName
                        FROM School sh
                        JOIN City c on sh.cityId = c.cityId
                        JOIN State s on sh.stateId = s.stateId
                        Join AccredAgency a on sh.agencyId = a.agencyId 
                        WHERE sh.cityId = @value AND sh.OutStateTuition >= @value2 AND sh.OutStateTuition <= @value3 ORDER BY c.cityName;";

            return SqlAccess.GetData<School>(sql, cityId, min, max);
        }

        public static List<School> GetSchoolByTuition(Double min, Double max)
        {

            string sql = @"SELECT sh.schoolId, c.cityName, s.stateName, a.agencyName, sh.schoolUrl, sh.aidUrl, sh.latitude, 
	                    sh.longitude, sh.adminRate, sh.satAvg, sh.actAvg, sh.distance, sh.inStateTuition, sh.outStateTuition, sh.schoolName
                        FROM School sh
                        JOIN City c on sh.cityId = c.cityId
                        JOIN State s on sh.stateId = s.stateId
                        Join AccredAgency a on sh.agencyId = a.agencyId 
                        WHERE sh.OutStateTuition >= @value AND sh.OutStateTuition <= @value2;";

            return SqlAccess.GetData<School>(sql, min, max);
        }

        public static School GetSchoolById(int id)
        {
            string sql = @"SELECT sh.schoolId, c.cityName, s.stateName, a.agencyName, sh.schoolUrl, sh.aidUrl, sh.latitude, 
	                    sh.longitude, sh.adminRate, sh.satAvg, sh.actAvg, sh.distance, sh.inStateTuition, sh.outStateTuition, sh.schoolName
                        FROM School sh
                        JOIN City c on sh.cityId = c.cityId
                        JOIN State s on sh.stateId = s.stateId
                        Join AccredAgency a on sh.agencyId = a.agencyId  WHERE sh.schoolId = @id";
            return SqlAccess.GetSingleEnity<School>(sql, id);
        }

        public static Double GetMinPrice()
        {
            string sql = "SELECT min(outStateTuition) FROM School WHERE outStateTuition > 0;";
            return SqlAccess.GetSingleEnity<Double>(sql, null);
        }
        public static Double GetMaxPrice()
        {
            string sql = "SELECT max(outStateTuition) FROM School WHERE outStateTuition > 0;";
            return SqlAccess.GetSingleEnity<Double>(sql, null);
        }

    }
}
