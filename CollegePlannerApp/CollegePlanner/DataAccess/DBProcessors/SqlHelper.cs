﻿using CollegePlanner.Model;
using System.Collections.Generic;

namespace CollegePlanner.DataAccess.DBProcessors
{
    /// <summary>
    /// SqlHelper provides the ability to pull simple list from the DB.
    /// Example state, city, apartment, and payscale list are all generated from SqlHelper.
    /// </summary>
    public class SqlHelper
    {
        public static List<State> GetStateList()
        {
            string sql = "SELECT * FROM State ORDER BY stateName;";

            return SqlAccess.GetData<State>(sql);
        }
        public static List<City> GetCityList(int stateId)
        {
            string sql = "SELECT * FROM City WHERE stateId = @searchValue ORDER BY cityName;";
            return SqlAccess.GetData<City>(sql, stateId);
        }
        public static List<Apartment> GetApartmentList()
        {
            string sql = "SELECCT * FROM Apartment;";
            return SqlAccess.GetData<Apartment>(sql);
        }
        public static List<Payscale> GetPayscaleList()
        {
            string sql = "SELECT * FROM Payscale;";
            return SqlAccess.GetData<Payscale>(sql);
        }
        public static Payscale GetPayscaleById(int id)
        {
            string sql = "SELECT * FROM Payscale WHERE payId = @value";
            return SqlAccess.GetSingleEnity<Payscale>(sql, id);
        }

    }
}
