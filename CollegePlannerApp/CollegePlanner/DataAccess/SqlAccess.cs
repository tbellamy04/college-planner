﻿using Dapper;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CollegePlanner.DataAccess
{
    public static class SqlAccess
    {

        public static string GetConnectionString(string name = "CollegePlannerDB")
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }

        public static List<T> GetData<T>(string sql, dynamic value = null, dynamic value2 = null, dynamic value3 = null)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                if (value == null)
                {
                    return cnn.Query<T>(sql).ToList();
                }
                else if (value2 == null)
                {
                    return cnn.Query<T>(sql, new { searchValue = value }).ToList();
                }
                else if (value3 == null)
                {
                    return cnn.Query<T>(sql, new { value, value2 }).ToList();
                }
                else
                {
                    return cnn.Query<T>(sql, new { value, value2, value3 }).ToList();
                }
            }
        }

        public static T GetSingleEnity<T>(string sql, dynamic value = null, dynamic value2 = null)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                if(value == null)
                {
                    try
                    {
                        return cnn.QuerySingle<T>(sql);
                    }
                    catch
                    {
                        return default(T);
                    }
                }
                else if (value2 == null)
                {
                    try
                    {
                        return cnn.QuerySingle<T>(sql, new { value = value });
                    }
                    catch
                    {
                        return default(T);
                    }
                }
                else
                {
                    try
                    {
                        return cnn.QuerySingle<T>(sql, new { value, value2 });
                    }
                    catch
                    {
                        return default(T);
                    }
                }
            }
        }

        public static int UpdataData<T>(string sql, T data)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                return cnn.Execute(sql, data);
            }
        }

    }
}
