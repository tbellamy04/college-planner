﻿using CollegePlanner.Controller;
using CollegePlanner.Model;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace CollegePlanner.View
{
    /// <summary>
    /// Interaction logic for UserAccountPage.xaml
    /// </summary>
    public partial class UserAccountPage : Page
    {
        AppUser appUser;
        List<Appointment> appoinments;
        MController controller = MController.Instance();
        public UserAccountPage()
        {
            InitializeComponent();
            appUser = controller.SignedInUser;
            appoinments = controller.UserAppointments;

            firstName.Text = appUser.FirstName;
            lastName.Text = appUser.LastName;

            // Decrypts username
            userName.Text = CollegePlanner.Model.Encryption.Decrypt(appUser.UserName);

            AppointmentGrid.DataContext = appoinments;
            payscaleCombo.DataContext = controller.GetPayscaleList();
            payscaleCombo.SelectedIndex = appUser.PayscaleId - 1;
            ModifyButton.IsEnabled = false;
            DeleteButton.IsEnabled = false;
        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new HomePage());
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new SearchPage());
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        // Checks if the users first name, last name, or major fields where changed.
        // If any of the 3 fields were change all 3 are updated in the DB. 
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (!(firstName.Text.Equals(appUser.FirstName))
                || !(lastName.Text.Equals(appUser.LastName))
                || (payscaleCombo.SelectedIndex != (appUser.PayscaleId - 1)))
            {
                appUser.FirstName = firstName.Text;
                appUser.LastName = lastName.Text;
                appUser.PayscaleId = ((Payscale)payscaleCombo.SelectedItem).PayId;
                controller.ModifyUser(appUser);
                MessageBox.Show("User account has been updated.");
            }
            else
            {
                MessageBox.Show("No changes made.");
            }
        }

        private void ModifyButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new ModifyAppointmentPage((Appointment)AppointmentGrid.SelectedItem));
        }

        private void AppointmentGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ModifyButton.IsEnabled = true;
            DeleteButton.IsEnabled = true;
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete this appointment", "Delete Conformation", MessageBoxButton.YesNo);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    controller.DeleteAppointment((Appointment)AppointmentGrid.SelectedItem);
                    controller.UserAppointments.Remove((Appointment)AppointmentGrid.SelectedItem);
                    NavigationService.Navigate(new UserAccountPage());
                    break;
                default:
                    break;
            }
        }
    }
}
