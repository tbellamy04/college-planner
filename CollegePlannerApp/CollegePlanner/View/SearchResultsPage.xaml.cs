﻿using CollegePlanner.Controller;
using CollegePlanner.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace CollegePlanner.View
{
    /// <summary>
    /// Interaction logic for SearchResultsPage.xaml
    /// </summary>
    public partial class SearchResultsPage : Page
    {
        public List<School> schools;
        MController controller = MController.Instance();
        public List<CareerEarnings> PayPerYear;
        public List<Payscale> payscales;
        public SearchResultsPage(List<School> results)
        {
            InitializeComponent();
            schools = results;
            schoolGrid.DataContext = schools;
            careerCombo.DataContext = controller.GetPayscaleList();
            careerCombo.SelectedIndex = controller.SignedInUser.PayscaleId - 1;
            GetEstimatedPayPerYear(controller.GetPayscaleById(controller.SignedInUser.PayscaleId));
            DetailButton.IsEnabled = false;
        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new HomePage());
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new SearchPage());
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void schoolGrid_SelectionChanged(object sender, Syncfusion.UI.Xaml.Grid.GridSelectionChangedEventArgs e)
        {
            DetailButton.IsEnabled = true;
            List<School> selectedSchools = new List<School>();
            foreach (var row in schoolGrid.SelectedItems)
            {
                selectedSchools.Add((School)row);
            }

            tuitionGraph.DataContext = selectedSchools;

        }
        private void careerCombo_DropDownClosed(object sender, EventArgs e)
        {
            GetEstimatedPayPerYear((Payscale)careerCombo.SelectedItem);
        }
        private void GetEstimatedPayPerYear(Payscale data)
        {
            List<Double> salaryLine = new List<double>();
            List<string> numYears = new List<string> { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11" };
            List<CareerEarnings> tempList = new List<CareerEarnings>();
            salaryLine.Add(data.StartingSalary);
            double raisePerYer = (data.MidSalary - data.StartingSalary) / 10;

            foreach (string year in numYears)
            {
                CareerEarnings career = new CareerEarnings
                {
                    YearNumber = year,
                    YearSalary = salaryLine.Last()
                };
                tempList.Add(career);
                salaryLine.Add(salaryLine.Last() + raisePerYer);
            }
            salaryGraph.DataContext = tempList;

        }

        private void DetailButton_Click(object sender, RoutedEventArgs e)
        {
            List<School> selectedSchools = new List<School>();

            foreach (var row in schoolGrid.SelectedItems)
            {
                selectedSchools.Add((School)row);
            }
            NavigationService.Navigate(new SchoolDetailPage(selectedSchools));
        }
    }
}
