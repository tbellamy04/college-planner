﻿using CollegePlanner.Controller;
using CollegePlanner.Model;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace CollegePlanner.View
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class HomePage : Page
    {
        public AppUser appUser;
        public List<Appointment> appointments;
        MController controller = MController.Instance();
        public HomePage()
        {
            InitializeComponent();
            appUser = controller.SignedInUser;
            appointments = controller.UserAppointments;

            AppointmentGrid.DataContext = appointments;
            DeleteButton.IsEnabled = false;
            ModifyButton.IsEnabled = false;
        }

        private void AccountButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new UserAccountPage());

        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new SearchPage());
        }
        private void AboutButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new AboutPage());

        }

        private void ShowAll_Click(object sender, RoutedEventArgs e)
        {
            AppointmentGrid.DataContext = appointments;
        }

        private void AppointmentCalendar_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            List<Appointment> sortedAppointment = new List<Appointment>();

            foreach (Appointment appointment in appointments)
            {
                if (appointment.AppointmentTime.Date == choosenDate.SelectedDate)
                {
                    sortedAppointment.Add(appointment);
                }
            }
            AppointmentGrid.DataContext = sortedAppointment;
        }

        private void Filter_Click(object sender, RoutedEventArgs e)
        {
            List<Appointment> sortedAppointment = new List<Appointment>();

            foreach (Appointment appointment in appointments)
            {
                if (appointment.AppointmentTime.Date == choosenDate.SelectedDate)
                {
                    sortedAppointment.Add(appointment);
                }
            }
            AppointmentGrid.DataContext = sortedAppointment;
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete this appointment", "Delete Conformation", MessageBoxButton.YesNo);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    controller.DeleteAppointment((Appointment)AppointmentGrid.SelectedItem);
                    controller.UserAppointments.Remove((Appointment)AppointmentGrid.SelectedItem);
                    NavigationService.Navigate(new HomePage());
                    break;
                default:
                    break;
            }
        }

        private void ModifyButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new ModifyAppointmentPage((Appointment)AppointmentGrid.SelectedItem));
        }

        private void AppointmentGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DeleteButton.IsEnabled = true;
            ModifyButton.IsEnabled = true;
        }
    }
}

