﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace CollegePlanner.View
{
    /// <summary>
    /// Interaction logic for AboutPage.xaml
    /// </summary>
    public partial class AboutPage : Page
    {
        public AboutPage()
        {
            InitializeComponent();
            aboutText.Text = "College Planner is an application created by DW Software Solutions to help future college students.\n" +
                              "The program gives prespective college students and their parents the power to quickly compare over 1,000 colleges in America.\n" +
                              "Out powerful program also allows the user to sign in and save furture campus visits.";
        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new HomePage());
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void HelpButton_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
