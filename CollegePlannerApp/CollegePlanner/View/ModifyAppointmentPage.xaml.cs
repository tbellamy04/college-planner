﻿using CollegePlanner.Controller;
using CollegePlanner.Model;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace CollegePlanner.View
{
    /// <summary>
    /// Interaction logic for ModifyAppointmentPage.xaml
    /// </summary>
    public partial class ModifyAppointmentPage : Page
    {
        Appointment appointment;
        MController controller = MController.Instance();
        public ModifyAppointmentPage(Appointment incomingAppointment)
        {
            InitializeComponent();
            appointment = incomingAppointment;
            schoolName.Text = appointment.SchoolName.ToString();
            schoolName.IsReadOnly = true;
            appointmentDate.Text = appointment.AppointmentTime.Date.ToString();
            appointmentDate.SelectedDate = appointment.AppointmentTime.Date;
            appoinmentTime.SelectedItem = appointment.AppointmentTime;
            notes.Text = appointment.Notes.ToString();
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (appoinmentTime.SelectedItem == null)
            {
                MessageBox.Show("Please a time for the upcoming appointment.");
            }
            else if (appointmentDate.SelectedDate == null)
            {
                MessageBox.Show("Please select a date for the upcoming appoinment.");
            }
            else
            {
                appointment.AppointmentTime = (DateTime)appoinmentTime.SelectedItem;
                appointment.Notes = notes.Text;

                if (controller.ModifyAppointment(appointment) >= 1)
                {
                    MessageBox.Show("Appointment has been successfully modify.");
                    NavigationService.GoBack();
                }
                else
                {
                    MessageBox.Show("Error in creating appointment please try again.");
                }
            }
        }

        private void appointmentDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            load_appointment_comboBox();
        }

        private void load_appointment_comboBox()
        {
            DateTime date = (DateTime)appointmentDate.SelectedDate;
            DateTime[] times = new DateTime[]
            {
                date.AddHours(8),
                date.AddHours(9),
                date.AddHours(10),
                date.AddHours(11),
                date.AddHours(12),
                date.AddHours(13),
                date.AddHours(14),
                date.AddHours(15),
                date.AddHours(16),
                date.AddHours(17)
            };
            appoinmentTime.ItemsSource = times;
        }
    }
}
