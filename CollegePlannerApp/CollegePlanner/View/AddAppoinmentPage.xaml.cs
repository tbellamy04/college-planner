﻿using CollegePlanner.Controller;
using CollegePlanner.Model;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace CollegePlanner.View
{
    /// <summary>
    /// Interaction logic for AddAppoinmentPage.xaml
    /// </summary>
    public partial class AddAppoinmentPage : Page
    {
        School school;
        MController controller = MController.Instance();
        public AddAppoinmentPage(School school)
        {
            InitializeComponent();
            this.school = school;
            schoolName.Text = school.SchoolName;
            appoinmentTime.IsEnabled = false;
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (appoinmentTime.SelectedItem == null)
            {
                MessageBox.Show("Please a time for the upcoming appointment.");
            }
            else if (appointmentDate.SelectedDate == null)
            {
                MessageBox.Show("Please select a date for the upcoming appoinment.");
            }
            else
            {
                Appointment appointment = new Appointment
                {
                    AppointmentTime = (DateTime)appoinmentTime.SelectedItem,
                    Notes = notes.Text,
                    SchoolId = school.SchoolId,
                    SchoolName = school.SchoolName,
                    UserName = controller.SignedInUser.UserName
                };

                if (controller.AddAppointment(appointment) >= 1)
                {
                    MessageBox.Show("Appointment has been successfully created.");
                    appointment.AppointmentId = controller.GetAppointmentId();
                    controller.UserAppointments.Add(appointment);
                    NavigationService.GoBack();
                }
                else
                {
                    MessageBox.Show("Error in creating appointment please try again.");
                }

            }

        }

        private void appointmentDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            appoinmentTime.IsEnabled = true;
            DateTime date = (DateTime)appointmentDate.SelectedDate;
            DateTime[] times = new DateTime[]
            {
                date.AddHours(8),
                date.AddHours(9),
                date.AddHours(10),
                date.AddHours(11),
                date.AddHours(12),
                date.AddHours(13),
                date.AddHours(14),
                date.AddHours(15),
                date.AddHours(16),
                date.AddHours(17)
            };
            appoinmentTime.ItemsSource = times;
        }

    }
}
