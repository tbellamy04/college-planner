﻿using CollegePlanner.Model;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace CollegePlanner.View
{
    /// <summary>
    /// Interaction logic for SchoolDetailPage.xaml
    /// </summary>
    public partial class SchoolDetailPage : Page
    {
        List<School> schools;
        public SchoolDetailPage(List<School> schools)
        {
            InitializeComponent();
            this.schools = schools;
            schoolGrid.DataContext = schools;
            AppointmentButton.IsEnabled = false;
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new SearchPage());

        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new HomePage());
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void schoolGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AppointmentButton.IsEnabled = true;
            School selectedSchool = (School)schoolGrid.SelectedItem;

            schoolName.Text = selectedSchool.SchoolName;
            cityName.Text = selectedSchool.CityName;
            stateName.Text = selectedSchool.StateName;
            if (selectedSchool.SchoolUrl.StartsWith("@"))
            {
                schoolUrl.Text = selectedSchool.SchoolUrl.TrimStart('@');
            }
            else
            {
                schoolUrl.Text = selectedSchool.SchoolUrl;
            }
            if (selectedSchool.AidUrl.StartsWith("@"))
            {
                aidUrl.Text = selectedSchool.AidUrl.TrimStart('@');
            }
            else
            {
                aidUrl.Text = selectedSchool.AidUrl;
            }
            adminRate.Text = (selectedSchool.AdminRate * 100).ToString() + "%";
            satAvg.Text = selectedSchool.SatAvg.ToString();
            actAvg.Text = selectedSchool.ActAvg.ToString();
        }

        private void AppointmentButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new AddAppoinmentPage((School)schoolGrid.SelectedItem));
        }
    }
}
