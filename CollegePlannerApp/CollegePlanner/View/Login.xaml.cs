﻿using CollegePlanner.Controller;
using CollegePlanner.Model;
using System;
using System.Windows;
using System.Windows.Input;

namespace CollegePlanner.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
        }

        private void DragWindow(object sender, MouseButtonEventArgs e)
        {
            try
            {
                this.DragMove();
            }
            catch
            {

            }
        }

        /// <summary>
        /// Takes the username and password fields and encrytps them then compares it to what is in the CollegePlanner DB.
        /// If no user is returned fromt CollegePlannerDB then either the username was not in the database or the password did not match.
        /// If a user is returned it is then passed to the main window / home screen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            string userName = CollegePlanner.Model.Encryption.Encyrpt(Username.Text);
            string password = CollegePlanner.Model.Encryption.Encyrpt(Password.Password.ToString());
            MController controller = MController.Instance();
            AppUser appUser = controller.GetUser(userName, password);

            if (appUser == null)
            {
                MessageBox.Show("Incorrect user or password entered");
            }
            else
            {
                // Setup the controller variables for this user
                controller.SignedInUser = appUser;
                controller.LoadAppointmentList();
                MainWindow newWindow = new MainWindow();
                newWindow.Show();
                Close();
            }
        }

        private void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            CreateAccount newCreateAccount = new CreateAccount();
            newCreateAccount.Show();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
