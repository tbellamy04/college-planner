﻿using CollegePlanner.Controller;
using CollegePlanner.Model;
using System.Windows;


namespace CollegePlanner.View
{
    /// <summary>
    /// Interaction logic for CreateAccount.xaml
    /// </summary>
    public partial class CreateAccount : Window
    {
        MController controller = MController.Instance();
        public CreateAccount()
        {
            InitializeComponent();
            payscaleCombo.DataContext = controller.GetPayscaleList();
        }
        /// <summary>
        /// Takes 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            if (ValidateData())
            {
                Payscale payscale = (Payscale)payscaleCombo.SelectedItem;
                AppUser appUser = new AppUser
                {
                    UserName = CollegePlanner.Model.Encryption.Encyrpt(Username.Text),
                    FirstName = Firstname.Text,
                    LastName = Lastname.Text,
                    Password = CollegePlanner.Model.Encryption.Encyrpt(Password.Password.ToString()),
                    PayscaleId = payscale.PayId
                };

                int created = controller.Adduser(appUser);

                if (created == -1)
                {
                    MessageBox.Show("Username already exist in database please choose something different.");
                    Username.Clear();
                    Password.Clear();
                }
                else if (created == 1)
                {
                    MessageBox.Show("Account succesfully created returning to login screen.");
                    Close();
                }
                else
                {
                    MessageBox.Show("Database error please try again.");
                    Password.Clear();
                }

            }
            else
            {
                Password.Clear();
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private bool ValidateData()
        {
            if (Username.Text.Length < 4)
            {
                MessageBox.Show("Invalid username enter. Username should be atleast 4 charcters long");
                return false;
            }
            if (Firstname.Text.Length == 0)
            {
                MessageBox.Show("Invalid first name entered");
                return false;
            }
            if (Lastname.Text.Length == 0)
            {
                MessageBox.Show("Invalid last name entered");
                return false;
            }
            if (Password.Password.Length < 5)
            {
                MessageBox.Show("Invalid password entered. Password should be atleast 5 caharcters long");
                return false;
            }
            if (payscaleCombo.SelectedItem == null)
            {
                MessageBox.Show("Please select a major that you are interested in. This field can be changed at anytime.");
                return false;
            }


            return true;
        }
    }
}
