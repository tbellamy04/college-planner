﻿using CollegePlanner.Controller;
using CollegePlanner.Model;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace CollegePlanner.View
{
    /// <summary>
    /// Interaction logic for SearchPage.xaml
    /// </summary>
    public partial class SearchPage : Page
    {
        public List<State> StateList { get; set; }
        public List<City> CityList { get; set; }

        MController controller = MController.Instance();
        public SearchPage()
        {
            InitializeComponent();
            StateList = controller.StatesList;
            stateCombo.DataContext = StateList;
            priceSlider.RangeStart = priceSlider.Minimum;
            priceSlider.RangeEnd = priceSlider.Maximum;
        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new HomePage());
        }

        private void AccountButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new UserAccountPage());
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            List<School> searchResults;
            if (cityGrid.SelectedItem != null)
            {
                City selectedCity = (City)cityGrid.SelectedItem;
                searchResults = controller.GetSchoolByCity(selectedCity.CityId, priceSlider.RangeStart, priceSlider.RangeEnd);
            }
            else if (stateCombo.SelectedItem != null)
            {
                State selectedState = (State)stateCombo.SelectedItem;
                searchResults = controller.GetSchoolByState(selectedState.StateId, priceSlider.RangeStart, priceSlider.RangeEnd);
            }
            else
            {
                searchResults = controller.GetSchoolByPrice(priceSlider.RangeStart, priceSlider.RangeEnd);
            }
            NavigationService.Navigate(new SearchResultsPage(searchResults));
        }

        private void stateCombo_DropDownClosed(object sender, EventArgs e)
        {
            State selectedState = (State)stateCombo.SelectedItem;
            try
            {
                CityList = controller.GetCitiesByState(selectedState.StateId);
            }
            catch
            {
                MessageBox.Show("Error loading cities");
            }
            cityGrid.DataContext = CityList;
        }
    }
}
