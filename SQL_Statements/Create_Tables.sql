CREATE TABLE City (
  cityId int NOT NULL UNIQUE IDENTITY(1,1),
  stateId int,
  cityName varchar(255),
  primary key (CityId)
);

CREATE TABLE Payscale (
  payId int NOT NULL UNIQUE IDENTITY(1,1),
  major varchar(255),
  startingSalary float,
  midSalary float,
  primary key (payId)
);

CREATE TABLE School (
  schoolId int NOT NULL UNIQUE IDENTITY(1,1),
  cityId int NOT NULL,
  stateId int NOT NULL,
  agencyId int NOT NULL,
  schoolUrl varchar(255),
  aidUrl varchar(255),
  latitude int,
  longitude int,
  adminRate Float,
  satAvg Float,
  actAvg Float,
  distance BOOLEAN,
  inStateTuition Float,
  outStateTuition Float,
  schoolName varchar(255),
  primary key (schoolId)
);

CREATE TABLE State (
  stateId int NOT NULL UNIQUE IDENTITY(1,1),
  stateName varchar(255) NOT NULL UNIQUE,
  primary key (stateId)
);

CREATE TABLE AccredAgency (
  agencyId int NOT NULL UNIQUE IDENTITY(1,1),
  agencyName varchar(255),
  primary key (agencyId)
);

CREATE TABLE Appointment(
	appointmentId int NOT NULL UNIQUE IDENTITY(1,1),
	schoolId int NOT NULL,
	userName varchar(255) NOT NULL,
	time DATETIME,
	notes varchar(255),
	primary key (appointmentId)
);

CREATE TABLE AppUser(
	userName varchar(255) NOT NULL UNIQUE,
	firstName varchar(255),
	lastName varchar(255),
	password varchar(255)
	primary key (userName)
);

CREATE INDEX FK ON  City (StateId);
CREATE INDEX FK ON  School (CityId, StateId, AgencyId);
CREATE INDEX FK ON Appointment (schoolId, userName);
CREATE INDEX FK ON AppUser (userName);
